#include <epuck.h>

/* Variables for threads and sockets */
typedef struct sockaddr_in SOCKADDR_IN;
SOCKADDR_IN SockaddrinInit, local_SockaddrinInit, SockaddrinCamera,
    SockaddrinReceptionCapteurs, local_SockaddrinEnvoieCommandes,
    SockaddrinEnvoieCommandes;
SOCKET SockInit;
typedef struct sockaddr SOCKADDR;

// General functions
void sigint_handler(int signal) {
    stop_threads = true;
}

inline int sign(float val) {
    if (val < 0)
        return -1;
    if (val == 0)
        return 0;
    return 1;
}

// Functions for opening sockets
void InitSocketOpening(const std::string& epuck_ip) {
    printf("Creating socket Init :\n\r");
    static const int PortsockInit = 1029;
    SockInit = socket(AF_INET, SOCK_DGRAM, 0);
    if (SockInit == INVALID_SOCKET) {
        printf("Desole, je ne peux pas creer la socket port: %d \n",
               PortsockInit);
    } else {
        printf("socket port %d : OK \n", PortsockInit);
        /* Lie la socket � une ip et un port d'�coute */
        // callee (epuck) -> distant socket where I need to send to epuck
        SockaddrinInit.sin_family = AF_INET;
        SockaddrinInit.sin_addr.s_addr =
            inet_addr(epuck_ip.c_str()); // IP de l'epuck
        SockaddrinInit.sin_port =
            htons(PortsockInit); // Ecoute ou emet sur le PORT
        // caller (local PC)
        local_SockaddrinInit.sin_family = AF_INET;
        local_SockaddrinInit.sin_addr.s_addr =
            htonl(INADDR_ANY); // IP de l'epuck
        local_SockaddrinInit.sin_port =
            htons(PortsockInit); // Ecoute ou emet sur le PORT
        int erreur = bind(SockInit, (SOCKADDR*)&local_SockaddrinInit,
                          sizeof(local_SockaddrinInit));
        if (erreur == SOCKET_ERROR) {
            perror("ERROR Sock INIT :");
            printf("Echec du Bind de la socket port : %d \n", PortsockInit);
        } else {
            printf("bind : OK port : %d \n", PortsockInit);
        }
    }
}
void OpenCameraSocket() {
    printf("Creation Camera socket :\n\r");
    CameraSocket = socket(AF_INET, SOCK_DGRAM, 0);
    static const int PortsockCamera = 1026;

    if (CameraSocket == INVALID_SOCKET) {
        printf("Desole, je ne peux pas creer la socket port: %d \n",
               PortsockCamera);
    } else {
        printf("socket port %d : OK \n", PortsockCamera);
        /* Lie la socket � une ip et un port d'�coute */
        SockaddrinCamera.sin_family = AF_INET;
        SockaddrinCamera.sin_addr.s_addr = htonl(INADDR_ANY); // IP
        SockaddrinCamera.sin_port =
            htons(PortsockCamera); // Ecoute ou emet sur le PORT
        int erreur = bind(CameraSocket, (SOCKADDR*)&SockaddrinCamera,
                          sizeof(SockaddrinCamera));
        if (erreur == SOCKET_ERROR) {
            printf("Echec du Bind de la socket port : %d \n", PortsockCamera);
        } else {
            printf("bind : OK port : %d \n", PortsockCamera);
        }
    }
}
void OpenSensorReceivingSocket() {
    printf("Creation socket ReceptionCapteurs :\n\r");
    static const int PortsockReceptionCapteurs = 1028;
    SensorReceivingSocket = socket(AF_INET, SOCK_DGRAM, 0);
    if (SensorReceivingSocket == INVALID_SOCKET) {
        printf("Desole, je ne peux pas creer la socket port: %d \n",
               PortsockReceptionCapteurs);
    } else {
        printf("socket port %d : OK \n", PortsockReceptionCapteurs);
        /* Lie la socket � une ip et un port d'�coute*/
        SockaddrinReceptionCapteurs.sin_family = AF_INET;
        SockaddrinReceptionCapteurs.sin_addr.s_addr = htonl(INADDR_ANY); // IP
        SockaddrinReceptionCapteurs.sin_port =
            htons(PortsockReceptionCapteurs); // Ecoute ou emet sur le PORT
        int erreur =
            bind(SensorReceivingSocket, (SOCKADDR*)&SockaddrinReceptionCapteurs,
                 sizeof(SockaddrinReceptionCapteurs));
        if (erreur == SOCKET_ERROR) {
            printf("Echec du Bind de la socket port : %d \n",
                   PortsockReceptionCapteurs);
        } else {
            printf("bind : OK port : %d \n", PortsockReceptionCapteurs);
        }
    }
}
/* Socket EnvoieCommandes */
void OpenCommandSendingSocket(const std::string& epuck_ip) {
    printf("Creation socket EnvoieCommandes :\n\r");
    static const int PortsockEnvoieCommandes = 1027;
    CommandSendingSocket = socket(AF_INET, SOCK_DGRAM, 0);
    if (CommandSendingSocket == INVALID_SOCKET) {
        printf("Desole, je ne peux pas creer la socket port: %d \n",
               PortsockEnvoieCommandes);
    } else {
        printf("socket port %d : OK \n", PortsockEnvoieCommandes);
        /* Lie la socket � une ip et un port d'�coute */
        // callee (epuck) -> distant socket where I need to send to epuck
        SockaddrinEnvoieCommandes.sin_family = AF_INET;
        SockaddrinEnvoieCommandes.sin_addr.s_addr =
            inet_addr(epuck_ip.c_str()); // IP
        SockaddrinEnvoieCommandes.sin_port =
            htons(PortsockEnvoieCommandes); // Ecoute ou emet sur le PORT
        // caller (local PC)
        local_SockaddrinEnvoieCommandes.sin_family = AF_INET;
        local_SockaddrinEnvoieCommandes.sin_addr.s_addr =
            htonl(INADDR_ANY); // IP de l'epuck
        local_SockaddrinEnvoieCommandes.sin_port =
            htons(PortsockEnvoieCommandes); // Ecoute ou emet sur le PORT
        int erreur = bind(CommandSendingSocket,
                          (SOCKADDR*)&local_SockaddrinEnvoieCommandes,
                          sizeof(local_SockaddrinEnvoieCommandes));
        if (erreur == SOCKET_ERROR) {
            printf("Echec du Bind de la socket port : %d \n",
                   PortsockEnvoieCommandes);
        } else {
            printf("bind : OK port : %d \n", PortsockEnvoieCommandes);
        }
    }
}
/**** Fonction de fermeture de socket ****/
void CloseSocket(int NOM_SOCKET) {
    shutdown(NOM_SOCKET, 2); // Ferme la session d'emmission et d'�coute
    close(NOM_SOCKET);       // Ferme la socket
}
void SendMotorAndLEDCommandToRobot(const char MotorCmd[15]) {
    char MotorAndLEDCommand[23], LEDCommand[9];
    // prepare LED commands
    char Led_1, Led_2, Led_3, Led_4, Led_5, Led_6, Led_7, Led_8;
    Led_1 = '1';
    Led_2 = '1';
    Led_3 = '1';
    Led_4 = '1';
    Led_5 = '1';
    Led_6 = '1';
    Led_7 = '1';
    Led_8 = '1'; // 1 for ON, 0 for OFF
    sprintf(LEDCommand, "%c%c%c%c%c%c%c%c", Led_1, Led_2, Led_3, Led_4, Led_5,
            Led_6, Led_7, Led_8);
    sprintf(MotorAndLEDCommand, "%s%s", LEDCommand, MotorCmd);
    printf("Command to be sent: %s\n", MotorAndLEDCommand);
    int Send;
    Send = sendto(CommandSendingSocket, MotorAndLEDCommand, 23, 0,
                  (struct sockaddr*)&SockaddrinEnvoieCommandes,
                  sizeof(SockaddrinEnvoieCommandes));
    if (Send < 0) {
        printf("Error sending commands\n");
    } else if (Send == 0) {
        printf("Error nothing sent\n");
    }
}
void ReceiveSensorMeasures() {
    int BytesReception;
    socklen_t Size = sizeof(SockaddrinReceptionCapteurs);
    memset(AllSensors, 0, 100);

    struct timeval timeout;
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;
    
    fd_set socks;
    FD_ZERO(&socks);
    FD_SET(SensorReceivingSocket, &socks);
    if (select(SensorReceivingSocket + 1, &socks, NULL, NULL, &timeout)) {
        BytesReception =
            recvfrom(SensorReceivingSocket, AllSensors, 100, 0,
                     (SOCKADDR*)&SockaddrinReceptionCapteurs, &Size);
        if (BytesReception < 0) {
            AllSensors[0] = 0;
            printf("SORRY No Data received\n");
        } else {
            AllSensors[BytesReception] = 0;
            if (BytesReception > 0) {
            } else {
                printf("WARNING No Data received!\n");
            }
        }
    } else {
        printf("TIMEOUT Sensor data reading\n");
    }
}
void SplitSensorMeasures() {
    if (AllSensors[0] == 0) {
        printf("nothing received, no message to manage\n");
        return;
    }
    int SizeBufferto, SizeCapteurs,
        index = -1; // taille des capteurs et emplacement du caractere pour la
                    // d�coupe : index
    char CharEncodeur[2][20], CharProx[10][5];
    char* ptr_pos = NULL;
    char* bufferto;
    /* Preparation de la memoire */
    for (int i = 0; i++; i < 2) {
        memset(CharEncodeur[i], 0, 20);
    }
    for (int i = 0; i++; i < 10) {
        memset(CharProx[i], 0, 5);
    }

    /* Decoupage progressif des valeurs des capteurs */
    bufferto = (char*)calloc(72, sizeof(char));

    ptr_pos = strstr(AllSensors, "q"); // on cherche l'index o� se trouve q
    ptr_pos ? index = ptr_pos - AllSensors + 2 : index = 0;

    memcpy(bufferto, &AllSensors[index], SizeBufferto = 72);
    ptr_pos =
        strstr(bufferto,
               ","); // on cherche l'index o� se trouve la premi�re virgule
    ptr_pos ? index = ptr_pos - bufferto : index = 0;
    memcpy(CharEncodeur[0], bufferto,
           index); // on copie depuis le d�but du Buffer sur une longueur
                   // correspondant � l'index cherch� pr�c�demment

    memcpy(AllSensors, &bufferto[index + 1],
           SizeCapteurs =
               72 - (index + 1)); // on "enleve" la partie d�j� trait�e
    ptr_pos = strstr(AllSensors, "n");
    ptr_pos ? index = ptr_pos - AllSensors : index = 0;
    memcpy(CharEncodeur[1], AllSensors, index);

    memcpy(bufferto, &AllSensors[index + 2],
           SizeBufferto = SizeCapteurs - (index + 2));
    ptr_pos = strstr(bufferto, ",");
    ptr_pos ? index = ptr_pos - bufferto : index = 0;
    memcpy(CharProx[0], bufferto, index);

    memcpy(AllSensors, &bufferto[index + 1],
           SizeCapteurs = SizeBufferto - (index + 1));
    ptr_pos = strstr(AllSensors, ",");
    ptr_pos ? index = ptr_pos - AllSensors : index = 0;
    memcpy(CharProx[1], AllSensors, index);

    memcpy(bufferto, &AllSensors[index + 1],
           SizeBufferto = SizeCapteurs - (index + 1));
    ptr_pos = strstr(bufferto, ",");
    ptr_pos ? index = ptr_pos - bufferto : index = 0;
    memcpy(CharProx[2], bufferto, index);

    memcpy(AllSensors, &bufferto[index + 1],
           SizeCapteurs = SizeBufferto - (index + 1));
    ptr_pos = strstr(AllSensors, ",");
    ptr_pos ? index = ptr_pos - AllSensors : index = 0;
    memcpy(CharProx[3], AllSensors, index);

    memcpy(bufferto, &AllSensors[index + 1],
           SizeBufferto = SizeCapteurs - (index + 1));
    ptr_pos = strstr(bufferto, ",");
    ptr_pos ? index = ptr_pos - bufferto : index = 0;
    memcpy(CharProx[4], bufferto, index);

    memcpy(AllSensors, &bufferto[index + 1],
           SizeCapteurs = SizeBufferto - (index + 1));
    ptr_pos = strstr(AllSensors, ",");
    ptr_pos ? index = ptr_pos - AllSensors : index = 0;
    memcpy(CharProx[5], AllSensors, index);

    memcpy(bufferto, &AllSensors[index + 1],
           SizeBufferto = SizeCapteurs - (index + 1));
    ptr_pos = strstr(bufferto, ",");
    ptr_pos ? index = ptr_pos - bufferto : index = 0;
    memcpy(CharProx[6], bufferto, index);

    memcpy(AllSensors, &bufferto[index + 1],
           SizeCapteurs = SizeBufferto - (index + 1));
    ptr_pos = strstr(AllSensors, ",");
    ptr_pos ? index = ptr_pos - AllSensors : index = 0;
    memcpy(CharProx[7], AllSensors, index);

    memcpy(bufferto, &AllSensors[index + 1],
           SizeBufferto = SizeCapteurs - (index + 1));
    ptr_pos = strstr(bufferto, ",");
    ptr_pos ? index = ptr_pos - bufferto : index = 0;
    memcpy(CharProx[8], bufferto, index);

    memcpy(AllSensors, &bufferto[index + 1],
           SizeCapteurs = SizeBufferto - (index + 1));
    ptr_pos = strstr(AllSensors, "\r");
    ptr_pos ? index = ptr_pos - AllSensors : index = 0;
    memcpy(CharProx[9], AllSensors, index);

    /* conversion des Char en Int */
    for (int i = 0; i < 10; i++) {
        ProxSensors[i] = atoi(CharProx[i]);
    }
    prevEncoderL = EncoderL;
    prevEncodeurR = EncoderR;
    EncoderL = atoi(CharEncodeur[0]);
    EncoderR = atoi(CharEncodeur[1]);
}
/**** fonction initialisation Camera ****/
void InitCamera(const std::string& epuck_ip) {
    InitSocketOpening(epuck_ip); // ouverture de la socket d'initialisation
    int Send;
    char CommandeInit[2];
    /* Mise en place de la commande d'init */
    if (CameraActive == true) {
        sprintf(CommandeInit, "1");
    }
    if (CameraActive == false) {
        sprintf(CommandeInit, "0");
    }
    printf("commande Init = %s\n", CommandeInit);
    /* Envoi commande d'init */
    Send = sendto(SockInit, CommandeInit, sizeof(CommandeInit), 0,
                  (struct sockaddr*)&SockaddrinInit, sizeof(SockaddrinInit));
    if (Send < 0) {
        printf("erreur sur l'envoi de l'Init\n");
    } else if (Send == 0) {
        printf("Envoi vide \n");
    }
    CloseSocket(SockInit); // fermeture de la socket d'initialisation
    // HERE (by robin) waiting a little time to avoid synchronizatipon problem
    // with server (socket to receive commands not created yet)
    printf("waiting server to be ready...\n");
    sleep(2);
}
/*****************/
/**** Threads ****/
/*****************/
/**** Thread for receiving camera data ****/
void* CameraReceptionThread(void* arg) {
    printf("Executing thread for receiving camera data\n\r");
    static const int img_msg_size = 57600;
    static const int img_msg_header = 8;
    int num_bytes = -2;
    socklen_t sinsize = sizeof(SockaddrinCamera);
    unsigned char* buffer = (unsigned char*)calloc(
        img_msg_size + img_msg_header, sizeof(unsigned char));
    memset(imgData.msg, 0x0, img_msg_size);
    char idImg_buf[2], idBlock_buf[2];
    memset(idImg_buf, 0, 2);
    memset(idBlock_buf, 0, 2);
    memset(buffer, 0, img_msg_size + img_msg_header);
    int i = 0;
    bool GotData = false;
    
    struct timeval timeout;
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;

    fd_set socks;
    FD_ZERO(&socks);
    FD_SET(CameraSocket, &socks);
    while (not stop_threads) {
		//int ret = select(CameraSocket + 1, &socks, NULL, NULL, &timeout);
        //if (ret > 0) {
            num_bytes = recvfrom(CameraSocket, (char*)buffer,
                                 img_msg_size + img_msg_header, 0,
                                 (SOCKADDR*)&SockaddrinCamera, &sinsize);
            //printf("received n bytes %d %d times\n",num_bytes,i++);
            if (num_bytes < 0) {
                if (!GotData) {
                    printf("Warning: no bytes received\n");
                    GotData = true;
                }
            } else {
                GotData = false;
                memcpy(idImg_buf, &buffer[0], 2);
                // printf("idimg : %s\n",idImg_buf);
                imgData.id_img = atoi(idImg_buf);
				//std::cout << "id_img: " << imgData.id_img << std::endl;
                memcpy(idBlock_buf, &buffer[3], 1);
                // printf("idBlock : %s\n",idBlock_buf);
                imgData.id_block = atoi(idBlock_buf);
				//std::cout << "id_block: " << imgData.id_block << std::endl;
                memcpy(&imgData.msg[(imgData.id_block) * img_msg_size],
                       &buffer[8], img_msg_size);
				//for(int i=0; i<10; ++i) {
				//	std::cout << (int)buffer[8+i] << " ";
				//}
		std::cout << std::endl;
                num_bytes = -1;
            }
        /*}
		else if(ret < 0) {
			printf("Select error camera thread: %d\n", errno);
		}*/
    }
	printf("Exiting camera thread\n");
    // close the thread
    (void)arg;
    pthread_exit(NULL);
}
// SPECIFIC FUNCTIONS
void SaveData(const string & data_folder) {
    FILE *eg, *ed, *p0, *p1, *p2, *p3, *p4, *p5, *p6, *p7, *p8, *p9;
    eg = fopen((data_folder + "eg.txt").c_str(), "a+");
    ed = fopen((data_folder + "ed.txt").c_str(), "a+");
    p0 = fopen((data_folder + "p0.txt").c_str(), "a+");
    p1 = fopen((data_folder + "p1.txt").c_str(), "a+");
    p2 = fopen((data_folder + "p2.txt").c_str(), "a+");
    p3 = fopen((data_folder + "p3.txt").c_str(), "a+");
    p4 = fopen((data_folder + "p4.txt").c_str(), "a+");
    p5 = fopen((data_folder + "p5.txt").c_str(), "a+");
    p6 = fopen((data_folder + "p6.txt").c_str(), "a+");
    p7 = fopen((data_folder + "p7.txt").c_str(), "a+");
    p8 = fopen((data_folder + "p8.txt").c_str(), "a+");
    p9 = fopen((data_folder + "p9.txt").c_str(), "a+");
    fprintf(eg, "%i\n", EncoderL);
    fprintf(ed, "%i\n", EncoderR);
    fprintf(p0, "%i\n", ProxSensors[0]);
    fprintf(p1, "%i\n", ProxSensors[1]);
    fprintf(p2, "%i\n", ProxSensors[2]);
    fprintf(p3, "%i\n", ProxSensors[3]);
    fprintf(p4, "%i\n", ProxSensors[4]);
    fprintf(p5, "%i\n", ProxSensors[5]);
    fprintf(p6, "%i\n", ProxSensors[6]);
    fprintf(p7, "%i\n", ProxSensors[7]);
    fprintf(p8, "%i\n", ProxSensors[8]);
    fprintf(p9, "%i\n", ProxSensors[9]);
    fclose(eg);
    fclose(ed);
    fclose(p0);
    fclose(p1);
    fclose(p2);
    fclose(p3);
    fclose(p4);
    fclose(p5);
    fclose(p6);
    fclose(p7);
    fclose(p8);
    fclose(p9);
}

/**** Send commands to the wheel motors for 10 seconds****/
void SetWheelCommands(struct timeval startTime, const int& speed_L,
                      const int& speed_R, char MotorCmd[15]) {
    struct timeval curTime;
    gettimeofday(&curTime, NULL);
    long int timeSinceStart =
        ((curTime.tv_sec * 1000000 + curTime.tv_usec) -
         (startTime.tv_sec * 1000000 + startTime.tv_usec)) /
        1000;
    printf("SetWheelCommands\ttimeSinceStart = %ld ms\n", timeSinceStart);
    if (timeSinceStart < 10000) {
        // Sends the command to the epuck motors
        sprintf(MotorCmd, "D,%d,%d", speed_L, speed_R);
    } else {
        sprintf(MotorCmd, "D,%d,%d", 0, 0);
    }
}



string createLogFolder() {
    char folder_name[128];
    time_t current_time;
    struct tm date;

    time(&current_time);
    date = *localtime(&current_time);

    // YYYY_MM_DD-hh:mm:ss
    strftime(folder_name, 128, "../logs/%Y_%m_%d-%X", &date);
    struct stat st = {0};

    std::string log_folder = folder_name;
    if (stat(log_folder.c_str(), &st) == -1) {
        mkdir(log_folder.c_str(), 0777);
    }

    std::string data_log_folder_;
    data_log_folder_ = log_folder + "/data/";
    if (stat(data_log_folder_.c_str(), &st) == -1) {
        mkdir(data_log_folder_.c_str(), 0777);
    }

    images_log_folder = log_folder + "/images/";
    if (stat(images_log_folder.c_str(), &st) == -1) {
        mkdir(images_log_folder.c_str(), 0777);
    }
    return(data_log_folder_);
}
