clear all
close all
clc

%Chargement des donn�es de xVis.txt et yVis.txt
xVis=importdata('./data/xVis.txt');
yVis=importdata('./data/yVis.txt');
xEnc=importdata('./data/xEnc.txt');
yEnc=importdata('./data/yEnc.txt');
dx=importdata('./data/dx.txt');
dy=importdata('./data/dy.txt');
t=importdata('./data/time.txt');
V=importdata('./data/v.txt');

%correction last value vision


X_m=importdata('./data/xEnc.txt');
Y_m=importdata('./data/yEnc.txt');
sigmaAcc=0.3;

[XfromSpeed, YfromSpeed]=EstimateFromSpeeds(xEnc,yEnc,dx,dy,t,0);

%% Filtre de Kalman

% Param�tres de l'�quation d'�tat
dt=t(1);
A=[1 0 dt 0;0 1 0 dt; 0 0 1 0; 0 0 0 1];
global T;
T=[(dt^2)/2 0; 0 (dt^2)/2; dt 0; 0 dt]; %Erreur enonc� transpos�e mauvais endroit, dimension 2 ???
C=eye(4);
sigma_acc=0.3;
Q=T'*T*sigma_acc;
R=[0.8 0 0 0;0 0.6 0 0;0 0 0.3 0; 0 0 0 0.2];

%InitState=[xEnc(1);yEnc(1);dx(1);dy(1)];
%initCov=0.5*ones(4,1);

t_init=3;
InitState=[XfromSpeed(t_init);YfromSpeed(t_init);dx(t_init);dy(t_init)];
%InitState=[xEnc(1);yEnc(1);dx(1);dy(1)];
initCov=0.5*ones(4,1);
yVis2=yVis-ones(size(yVis,1),1)*(yVis(3)-YfromSpeed(3));

[ XKal, YKal, CovKalman ] = EstimateFromKalman ( InitState, xVis, yVis2, dx, dy, t, initCov, sigma_acc);

[ XKalet, YKalet, CovKalman ] = EstimateFromExtKalman ( InitState, xVis, yVis2, V, t, initCov, sigmaAcc);


%% Plot

plot(xVis(2:43),yVis2(2:43))
hold on
plot(XfromSpeed,YfromSpeed)
hold on
plot(XKal(2:43),YKal(2:43),'+')
hold on
plot(XKalet(2:43),YKalet(2:43),'+')

legend('Visuel','X et Y encodeur', 'Kalman lin�aire', 'Kalman non lin�aire')
xlabel('x');
ylabel('y');
