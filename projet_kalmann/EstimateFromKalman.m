function [ XKalet, YKalet, CovKalman ] = EstimateFromKalman ( InitState, Xvis, Yvis, DX, DY, t, initCov, sigmaAcc)
%state kalman is [x y dot{x} dot{y}]
%initCov must be a 4 element column vector
%initial state and covariance
StateKalman(1:4,1) = InitState; %try [X_m(1); Y_m(1); DX(1); DY(1)];
    XKalet(1) = StateKalman(1, 1);
    YKalet(1) = StateKalman(2, 1);
CovKalman(1:4, 1:4, 1) = diag(initCov)
%carachteristic matrices
C = eye(4);
deltaT=t(1)
A =[1 0 deltaT 0;0 1 0 deltaT; 0 0 1 0; 0 0 0 1]; ;
global T;
Q =T*T'*sigmaAcc 
R =[0.8 0 0 0;0 0.6 0 0;0 0 0.3 0; 0 0 0 0.2]; 
for I = 2 : (size(t, 1) )
    %prediction 
    %celle de l etat est identique a la premiere question du TP lorsque on
    %ne compte pas l'acceleration
    StateKalman(1:4, I) = A*StateKalman(1:4,I-1);
    CovKalman(1:4, 1:4, I) = A*CovKalman(1:4,1:4,I-1)*A'+Q;
    %mesure
    Y(1:4, I) =C*[Xvis(I); Yvis(I); DX(I); DY(I)];
    %mise a jour
    resY(1:4, I) =Y(1:4,I)-C*StateKalman(1:4,I);
    S(1:4, 1:4) =C*CovKalman(1:4, 1:4, I)*C'+R ;
    K(1:4, 1:4) =CovKalman(1:4, 1:4, I)*C'*inv(S);%attention: nul si CovKalman=0 
    StateKalman(1:4, I) = StateKalman(1:4, I)+K*resY(1:4,I) ;
    CovKalman(1:4, 1:4, I) = (eye(4)-K*C)*CovKalman(1:4, 1:4, I) ;
    XKalet(I) = StateKalman(1, I);
    YKalet(I) = StateKalman(2, I);
end

end

