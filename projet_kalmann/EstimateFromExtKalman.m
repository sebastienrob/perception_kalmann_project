function [ XKal, YKal, CovKalman ] = EstimateFromExtKalman ( InitState, X_m, Y_m, V, t, initCov, sigmaAcc)
%state kalman is [x y dot{x} dot{y}]
%initCov must be a 4 element column vector
%InitState must be a 4 element column vector
%initial state and covariance
StateKalman(1:4,1) = InitState; %try [X_m(1); Y_m(1); DX(1); DY(1)];
XKal(1) = StateKalman(1, 1);
YKal(1) = StateKalman(2, 1);
CovKalman(1:4, 1:4, 1) = diag(initCov); %attention doit pas etre nul
%carachteristic matrices
deltaT=t(1);
A =[1 0 deltaT 0;0 1 0 deltaT; 0 0 1 0; 0 0 0 1]; 
global T;
Q =T*T'*sigmaAcc 
R =[0.8 0 0 ;0 0.6 0 ;0 0 0.6]; 

for I = 2 : (size(t, 1) )
    %prediction 
    %celle de l etat est identique a la premiere question du TP lorsque on
    %ne compte pas l'acceleration
    StateKalman(1:4, I) = A * StateKalman(1:4, I-1);
    CovKalman(1:4, 1:4, I) = A * CovKalman(1:4, 1:4, I-1) * A' + Q;
    %mesure
    Y(1:3, I) = [X_m(I); Y_m(I); V(I)];
    %mise a jour
    resY(1:3,I)= Y(1:3,I) - [StateKalman(1,I); StateKalman(2,I); sqrt((StateKalman(3,I)^2)+(StateKalman(4,I)^2))];
    % V(I) =  ;
    G = [1 0 0 0;
        0 1 0 0;
        0 0 (StateKalman(3,I)/sqrt((StateKalman(3,I)^2)+(StateKalman(4,I)^2))) (StateKalman(4,I)/sqrt((StateKalman(3,I)^2)+(StateKalman(4,I)^2)))];
    S(1:3, 1:3) =G*CovKalman(1:4,1:4,I)*G' + R ;
    K(1:4, 1:3) = CovKalman(1:4,1:4,I)*G'*inv(S) ;%attention: nul si CovKalman=0 
    StateKalman(1:4, I) = StateKalman(1:4, I)+K*resY(1:3,I) ;
    CovKalman(1:4, 1:4, I) = (eye(4) - K*G)*CovKalman(1:4,1:4,I) ;
    XKal(I) = StateKalman(1, I);
    YKal(I) = StateKalman(2, I);
end

end

